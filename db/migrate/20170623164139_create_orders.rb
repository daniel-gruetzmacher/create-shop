class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :name
      t.string :street
      t.integer :zipcode
      t.string :city
      t.string :email
      t.integer :blz
      t.bigint :account
      t.string :status

      t.timestamps
    end
  end
end
