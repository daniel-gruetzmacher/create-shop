Rails.application.routes.draw do
  root 'products#shop'

  get 'shop', to: 'products#shop'
  get 'cart', to: 'cart#index'

  post 'add_to_cart', to: 'order_item#create'
  post 'update_cart', to: 'order_item#update'
  post 'remove_cart_item', to: 'order_item#destroy'

  get 'submit_order/step/:current_step', to: 'orders#submit', as: 'submit_order'
  patch 'submit_order/step/:current_step', to: 'orders#advance_step', as: 'advance_step'


  resources :orders
  resources :products
end
