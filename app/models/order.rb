class Order < ApplicationRecord
    attr_accessor :current_step

    has_many :order_items, :dependent => :delete_all

    validates :name, presence: true, if: -> { is_step(1) }
    validates :street, presence: true, if: -> { is_step(1) }
    validates :zipcode, presence: true, 
                        :numericality => { :greater_than_or_equal_to => 0 },
                        if: -> { is_step(1) }
    validates :city, presence: true, if: -> { is_step(1) }
    validates :blz, presence: true, 
                    length: { minimum: 5 }, 
                    :numericality => { :greater_than_or_equal_to => 0 },
                    if: -> { is_step(2) }
    validates :account, presence: true,
                        length: { minimum: 10 },
                        :numericality => { :greater_than_or_equal_to => 0 },
                        if: -> { is_step(2) }

    def total
        total = 0

        self.order_items.each do |item|
            total += ( item.product.price ) * item.quantity
        end

        return total
    end

    def is_step(step)
        step == current_step or current_step == nil
    end
end
