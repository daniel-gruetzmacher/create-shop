class Product < ApplicationRecord
  has_many :order_items, :dependent => :delete_all

  validates :title, presence: true
  validates :description, presence: true
  validates :price, presence: true, :numericality => { :greater_than_or_equal_to => 0 }
end
