class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  protected
    def cart_order
        if session[:order_id]
          @order = Order.find_by_id(session[:order_id])
        end

        if @order.nil?
            @order = self.new_cart
        end

        return @order
    end

    def new_cart
        @order = Order.new
        @order.status = 'pending'
        @order.save(:validate => false)
        session[:order_id] = @order.id

        return @order
    end
end
