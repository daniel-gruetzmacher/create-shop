class OrdersController < ApplicationController
  def index
    @orders = Order.where('status': 'completed').all
  end

  def create
    @order = Order.new(order_params)
 
    if @order.save
      redirect_to @order
    else
      render 'new'
    end
  end

  def destroy
    @order = Order.find(params[:id])
    @order.destroy
 
    redirect_to orders_path
  end

  def submit
    @order = cart_order

    if @order.order_items.count == 0
        redirect_to shop_path
    end
  end

  def advance_step
    @order = cart_order

    @order.current_step = params[:current_step].to_i
    @order.save

    if params[:current_step].to_i >= 3
        @order.status = 'completed'
        @order.save
        session[:order_id] = nil
        redirect_to orders_path
        return
    end

    next_step = params[:current_step].to_i + 1

    if @order.update(order_params)
      redirect_to submit_order_path(next_step)
    else
      render 'submit'
    end
  end

  private
    def order_params
      params.require(:order).permit(
        :street, 
        :city,
        :zipcode,
        :email,
        :name,
        :blz,
        :account
        )
    end
end
