class OrderItemController < ApplicationController
  def create
    @order = cart_order

    @order_item = @order.order_items.where('product_id': params[:order_item][:product_id]).first

    if @order_item
        quantity = @order_item.quantity + 1

        @order_item.update('quantity': quantity)
        @order_item.save
    else
        @order.order_items.new(order_items_params)
    end

    @order.save(:validate => false)

    redirect_to cart_path
  end

  def update
    if session[:order_id]
      @order = Order.find(session[:order_id])
      @order_item = OrderItem.find(params[:order_item][:id])

      if params[:order_item][:quantity] == '0'
        @order_item.destroy
        redirect_to cart_path
        return
      end

      @order_item.update(order_items_params)
    end 

    redirect_to cart_path
  end

  def destroy
    @order_item = OrderItem.find(params[:order_item][:id])
    @order_item.destroy
 
    redirect_to cart_path
  end


  private
    def order_items_params
        params.require(:order_item).permit(:product_id, :quantity)
    end
end
